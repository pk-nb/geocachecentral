﻿//  Form Sub
//
//

// Application Level Errors to display to user
function displayError(errorText) {
    $('#form-error').show();
    $('#form-error-text').text(errorText);
}
// Application Level Info to display to user
function displayInfo(infoText) {
    $('#form-info').show();
    $('#form-info-text').text(infoText);
}

// Hide errors, used after success
function dismissError() {
    $('#form-error').hide();
    $('#form-info').hide();
}


$(function () {
   
    // GoTo Form
    //
    // Jumps to specified coordinates in form. Can be in DMS or Decimal format.
    // Gets values latitude and longitude and attempts to parse and validate them.
    // If successful, pans google maps to location. Otherwise displays error to user.
    $("body").on("submit", "#goto-form", function (event) {
        event.preventDefault();

        var lat = $('#goto-form .latitude').val();
        var lon = $('#goto-form .longitude').val();

        parseLatLon(lat);
        parseLatLon(lon);

        // Error Checking 
        if (lat === "" || lon === "") {
            displayError("Missing latitude or longitude input");
        }
        else {
            dismissError(); // Hide Error if fixed

            // Query googlemaps
            var latlon = new google.maps.LatLng(lat, lon);
            window.map.panTo(latlon);
            console.log(window.map.getZoom());
            $('#goto-popover').popover('toggle')

        }
    });


    // Search State Form
    //
    // Requests cache data from server through geocacheController 
    // with getJSON. If successful, fills map with cache data
    // and pans map to show all points.
    $("body").on("submit", "#search-state-form", function (event) {
        event.preventDefault();

        // Get statename from form field
        var stateName = $('#stateForm').val();

        // Get Data from server, through apicontroller {controller}, {method}
        $.getJSON("api/geocache/GetCachesByStateName?stateName=" + stateName, function (data) {
            console.log(data.length);
            if (data.length === 0) {
                displayInfo("No caches in search range \"" + stateName + "\" found");
            } else {
                dismissError(); // Hide error if reenterred and found
                placeMarkersAndPan(data);   // Place markers
            }
         })
        .fail(function (jqXHR, textStatus, err) {
            displayError("Could not complete requested search. Server item not found.")
        });

        // Toggle Popover away
        $('#search-popover').popover('toggle')
    }); // End state search form


    // Search Zip3 Form
    //
    // Requests cache data from server through geocacheController 
    // with getJSON. If successful, fills map with cache data
    // and pans map to show all points.
    $("body").on("submit", "#search-zip3-form", function (event) {
        event.preventDefault();

        // Get zip3 from form field
        var zip3 = $('#zipForm').val();

        // Get Data from server, through apicontroller {controller}, {method}
        $.getJSON("api/geocache/GetCachesByZip3?zip3=" + zip3, function (data) {
            console.log(data.length);
            if (data.length === 0) {
                displayInfo("No caches in search range \"" + zip3 + "\" found");
            } else {
                dismissError(); // Hide error if reenterred and found
                placeMarkersAndPan(data);   // Place markers
            }
        })
        .fail(function (jqXHR, textStatus, err) {
            displayError("Could not complete requested search. Server item not found.")
        });

        // Toggle Popover away
        $('#search-popover').popover('toggle')
    }); // End zip3 search form



    // Search County Form
    //
    // Requests cache data from server through geocacheController 
    // with getJSON. If successful, fills map with cache data
    // and pans map to show all points.
    $("body").on("submit", "#search-county-form", function (event) {
        event.preventDefault();

        // Get county from field
        var countyName = $('#countyForm').val();

        // Get Data from server, through apicontroller {controller}, {method}
        $.getJSON("api/geocache/GetCachesByCounty?countyName=" + countyName, function (data) {
            console.log(data.length);
            if (data.length === 0) {
                displayInfo("No caches in search range \"" + countyName + "\" found");
            } else { // Success!
                dismissError(); // Hide error if reenterred and found
                placeMarkersAndPan(data);   // Place markers
            }
        })
        .fail(function (jqXHR, textStatus, err) {
            displayError("Could not complete requested search. Server item not found.")
        });

        // Toggle Popover away
        $('#search-popover').popover('toggle')
    }); // End county search form


    // Add CacheForm
    //
    // Posts data from form to AddNewGeocache method on server.
    // Validates Lat Lon, all other fields are required.
    $("body").on("submit", "#add-form", function (event) {
        
        event.preventDefault();

        //  Form validation
        var lat = $('#add-latitude').val();
        var lon = $('#add-longitude').val();

        // Return if error
        if ((lat > 85 || lat < -85)  || (lon > 180 || lon < -180)) {
            displayError("Invalid latitude / longitude");
            return;
        }

        var values = {
            "Latitude": $('#add-latitude').val(),
            "Longitude": $('#add-longitude').val(),
            "Title": $('#add-title').val(),
            "Description": $('#add-description').val(),
            "DifficultyRating": $('#add-difficulty-rating').val(),
            "SizeRating": $('#add-size-rating').val()

        }
        //console.log(JSON.stringify(values));


        // Post geocache object to server
        // On success, get geocache object again and pan & zoom to it
        // If failure display error message
        $.ajax({
            type: 'POST',
            url: 'api/geocache/AddNewGeocache',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(values),
            success: function (data) {
                //console.log(data);
                placeMarkersAndPan(data);
                $('#add-popover').popover('toggle') // Toggle Popover away
            },
            failure: function (jqXHR, textStatus, errorThrown) {
                displayError(errorThrown);
            }
        });

    }); // End add form
});

