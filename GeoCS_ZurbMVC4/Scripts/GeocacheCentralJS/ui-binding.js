﻿$(function () {
    // Button-Popover Binding through jQuery and Twitter Bootstrap
    $('#goto-popover').popover({
        html: true,
        placement: 'top',
        content: function () {
            return $('#goto-popover-content').html();
        }
    });

    $('#search-popover').popover({
        html: true,
        placement: 'top',
        content: function () {
            return $('#search-popover-content').html();
        }
    });

    $('#add-popover').popover({
        html: true,
        placement: 'top',
        content: function () {
            return $('#add-popover-content').html();
        }
    });

    $('#expeditions-popover').popover({
        html: true,
        placement: 'top',
        content: function () {
            return $('#expeditions-popover-content').html();
        }
    });
});

// Hide Error Messages on close button
$('.alert .close').on('click', function () {
    $(this).parent().hide();
});