﻿// Initialize Map with current location and markers
function initialize() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLocation, defaultLocation);
    }
    else {
        initMap([-34.397, 150.644]);
    }
}

// Function that initializes map to current (or default if no geolocation) location
function showLocation(position) {
    if (position.coords) {
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;
    } else {
        var lat = position[0];
        var lon = position[1];
    }
    //console.log(lat, lon);
    var latlon = new google.maps.LatLng(lat, lon);
    var mapOptions = {
        center: latlon,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL }
    };
    window.map = new google.maps.Map(document.getElementById("map-canvas"),
        mapOptions);

    var marker = new google.maps.Marker({
        position: latlon,
        map: map,
        icon: "../../Images/markers/current.png",
        title: "Current Location",
        zIndex: 288000001
    });
}

function defaultLocation(error) {
    console.log("Location disallowed, fill default");
    initMap([-34.397, 150.644]);
}


// Places data into infowindow 
function generateCacheInfoWindowHTML(cacheObject) {

    var ratingsString = "";

    if (cacheObject.DifficultyRating) ratingsString += '<li>Difficulty: ' + cacheObject.DifficultyRating + '</li>';
    else ratingsString += '<li>Difficulty: ?</li>';

    if (cacheObject.SizeRating) ratingsString += '<li>Cache Size: ' + cacheObject.SizeRating + '</li>';
    else ratingsString += '<li>Cache Size: ?</li>';

    if (cacheObject.PopularityRating) ratingsString += '<li>Popularity: ' + cacheObject.PopularityRating + '</li>';
    else ratingsString += '<li>Popularity: ?</li>';

    var contentString = '<div class="infobox">' +
                        '<p><strong>' + cacheObject.Title + '</strong></p>' +
                        '<p>' + cacheObject.Description + '</p>' +
                        '<ul>' + ratingsString + '</ul>' +
                        '</div>';


    return contentString;
}

// Make Geocache Marker
//
// Gets marker image resource based on attributes
function createGeocacheMarker(geocache, latlong) {

    var markerURL = '../../Images/markers/';
    var imageString = "";

    // Specify image resourse based on geocache ratings
    if (geocache.isStaffFavorite === true) {
        imageString += "star-";
    } else {
        // Cache Ranges
        var s = geocache.SizeRating;
        if (s <= 1) { imageString += "m-" }               // micro
        else if (s > 1 && s <= 2) { imageString += "s-" } // small
        else if (s > 2 && s <= 3) { imageString += "r-" } // regular
        else if (s > 3 && s <= 4) { imageString += "l-" } // large
        else { imageString += "o-" }                      // other
    }

    // Get difficulty color
    var d = geocache.DifficultyRating;
    if (d <= 1) { imageString += "1" }               // difficulty 1
    else if (d > 1 && d <= 2) { imageString += "2" } // 2
    else if (d > 2 && d <= 3) { imageString += "3" } // 3
    else if (d > 3 && d <= 4) { imageString += "4" } // 4
    else { imageString += "5" }                      // 5

    imageString += ".png";

    var marker = new google.maps.Marker({
        position: latlong,
        map: window.map,
        title: geocache.Title,
        icon: markerURL + imageString
    });

    return marker;
}


// Place Markers and Pan
//
// Takes list of objects that must have valid Latitude
// and Longitude values. Places markers and moves maps bounds
function placeMarkersAndPan(list) {
    var bounds = new google.maps.LatLngBounds();
    var markerArray = [];
    var infoBoxArray = [];

    for (var i = 0; i < list.length; i++) {
        // Place marker
        var latlong = new google.maps.LatLng(list[i].Latitude, list[i].Longitude)

        var marker = createGeocacheMarker(list[i], latlong);

        markerArray.push(marker);

        var infoHTML = generateCacheInfoWindowHTML(list[i]);
        console.log(infoHTML);

        var infobox = new InfoBox({
            content: infoHTML,
            disableAutoPan: false,
            maxWidth: 150,
            pixelOffset: new google.maps.Size(-140, 0),
            zIndex: null,
            boxStyle: {
                background: "url('../../Images/tipbox.gif') no-repeat",
                opacity: 0.75,
                width: "280px"
            },
            closeBoxMargin: "12px 4px 2px 2px",
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1),
        });

        markerArray[i].infobox = infobox;
        infoBoxArray.push(infobox);

        // Bind infobox to marker
        // Magic anonymous function taken from http://jsfiddle.net/ccJ9p/7/
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                markerArray[i].infobox.open(map, this);
            }
        })(marker, i));

        // Add latlong to bounds for pan & zoom
        bounds.extend(latlong);
    }

    map.fitBounds(bounds);
}

