﻿// Clean up parseLatLon
function parseLatLon(string) {

    var regexDMS = /(\d*)d(?: (\d*)'(?: (\d*(?:\.\d*)?)(?:''|"))?)?(?: (N|E|W|S))?/;
    var regexDecimal = /(-?\d+.\d*)(?: ?(N|E|W|S))?/;
    var regexNEWS = /(?:(N|E|W|S))/;

    if (regexDMS.test(string)) {
        var match = regexDMS.exec(string);
        console.log(match);
    } else if (regexDecimal.test(string)) {
        var match = regexDecimal.exec(string);
        for (var i = 0; i < match.length; ++i) {
            console.log(match[i]);
        }
    }
}

///[\d*]|[\d*.\d]|[NSEW]
// (\d*)d(?: (\d*)'(?: (\d*(?:\.\d*)?)(?:''|"))?)?(?: (NW|NE|SW|SE|N|E|W|S))?