﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GeoCS_ZurbMVC4.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class GeocacheCentralEntities : DbContext
    {
        public GeocacheCentralEntities()
            : base("name=GeocacheCentralEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<cacheReview> cacheReviews { get; set; }
        public DbSet<expedition> expeditions { get; set; }
        public DbSet<expeditionCach> expeditionCaches { get; set; }
        public DbSet<geocache> geocaches { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<wgs84_county> wgs84_county { get; set; }
        public DbSet<wgs84_states> wgs84_states { get; set; }
        public DbSet<wgs84_zip3> wgs84_zip3 { get; set; }
        public DbSet<expeditionStat> expeditionStats { get; set; }
    
        [EdmFunction("GeocacheCentralEntities", "findByCounty")]
        public virtual IQueryable<findByCounty_Result> findByCounty(string countyName)
        {
            var countyNameParameter = countyName != null ?
                new ObjectParameter("countyName", countyName) :
                new ObjectParameter("countyName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<findByCounty_Result>("[GeocacheCentralEntities].[findByCounty](@countyName)", countyNameParameter);
        }
    
        [EdmFunction("GeocacheCentralEntities", "findByState")]
        public virtual IQueryable<findByState_Result> findByState(string stateName)
        {
            var stateNameParameter = stateName != null ?
                new ObjectParameter("stateName", stateName) :
                new ObjectParameter("stateName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<findByState_Result>("[GeocacheCentralEntities].[findByState](@stateName)", stateNameParameter);
        }
    
        [EdmFunction("GeocacheCentralEntities", "findByZip3")]
        public virtual IQueryable<findByZip3_Result> findByZip3(string zip3)
        {
            var zip3Parameter = zip3 != null ?
                new ObjectParameter("zip3", zip3) :
                new ObjectParameter("zip3", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<findByZip3_Result>("[GeocacheCentralEntities].[findByZip3](@zip3)", zip3Parameter);
        }
    }
}
