//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GeoCS_ZurbMVC4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class expeditionCach
    {
        public int ExpeditionID { get; set; }
        public int GeocacheID { get; set; }
        public Nullable<int> OrderNumber { get; set; }
    
        public virtual expedition expedition { get; set; }
        public virtual geocache geocache { get; set; }
    }
}
