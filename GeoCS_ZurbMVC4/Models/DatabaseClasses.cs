﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoCS_ZurbMVC4.Models
{
    public class DatabaseClasses
    {
    }

    public class Geocache {

        public int GeocacheID { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int OwnerID { get; set; }
        public string Title { get; set; }
        public bool isPublic { get; set; }
        public Nullable<bool> isStaffFavorite { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> DifficultyRating { get; set; }
        public Nullable<decimal> SizeRating { get; set; }
        public Nullable<decimal> PopularityRating { get; set; }

        public Geocache() { }

        //public Geocache(findByState_Result g) {
        //    GeocacheID = g.GeocacheID;
        //    Latitude = g.Latitude;
        //    Longitude = g.Longitude;
        //    OwnerID = g.OwnerID;
        //    Title = g.Title;
        //    isPublic = g.isPublic;
        //    isStaffFavorite = g.isStaffFavorite;
        //    DifficultyRating = g.DifficultyRating;
        //    SizeRating = g.SizeRating;
        //    PopularityRating = g.PopularityRating;
        //}

    }
}