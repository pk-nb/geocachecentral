﻿using System.Web;
using System.Web.Mvc;

namespace GeoCS_ZurbMVC4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}