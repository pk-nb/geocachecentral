﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GeoCS_ZurbMVC4.Models;
using Newtonsoft.Json;

namespace GeoCS_ZurbMVC4.Controllers
{
    public class GeocacheController : ApiController
    {

        // GetAllCaches
        //
        // Returns all availible geocaches in the database. May be a lengthy query in future.
        // APIController will return JSon to forms.js which asks for geocaching data.
        [HttpGet]
        public List<Geocache> GetAllCaches()
        {

            GeocacheCentralEntities db = new GeocacheCentralEntities();
            var x = db.geocaches
                    .Select(g => new Geocache
                    {
                        GeocacheID = g.GeocacheID,
                        Latitude = g.Latitude,
                        Longitude = g.Longitude,
                        OwnerID = g.OwnerID,
                        Title = g.Title,
                        Description = g.Description,
                        isPublic = g.isPublic,
                        isStaffFavorite = g.isStaffFavorite,
                        DifficultyRating = g.DifficultyRating,
                        SizeRating = g.SizeRating,
                        PopularityRating = g.PopularityRating
                    }
                    ).ToList();

            return x;

        }

        // GetCachesByStateName
        //
        // Returns all geocaches within a state using SQL function through Entity Framework.
        // APIController will return JSon to forms.js which asks for geocaching data.
        [HttpGet]
        public List<Geocache> GetCachesByStateName(string stateName)
        {
            GeocacheCentralEntities db = new GeocacheCentralEntities();
            var x = db.findByState(stateName)
                    .Select(g => new Geocache {
                        GeocacheID = g.GeocacheID,
                        Latitude = g.Latitude,
                        Longitude = g.Longitude,
                        OwnerID = g.OwnerID,
                        Title = g.Title,
                        Description = g.Description,
                        isPublic = g.isPublic,
                        isStaffFavorite = g.isStaffFavorite,
                        DifficultyRating = g.DifficultyRating,
                        SizeRating = g.SizeRating,
                        PopularityRating = g.PopularityRating
                    }
                    ).ToList();

            if (x.Any()) return x;
            else return new List<Geocache>(); // return blank list for error handling
        }

        // GetCachesByZip3Name
        //
        // Returns all geocaches within a state using SQL function through Entity Framework.
        // APIController will return JSon to forms.js which asks for geocaching data.
        [HttpGet]
        public List<Geocache> GetCachesByZip3(string zip3)
        {
            GeocacheCentralEntities db = new GeocacheCentralEntities();
            var x = db.findByZip3(zip3)
                    .Select(g => new Geocache
                    {
                        GeocacheID = g.GeocacheID,
                        Latitude = g.Latitude,
                        Longitude = g.Longitude,
                        OwnerID = g.OwnerID,
                        Title = g.Title,
                        Description = g.Description,
                        isPublic = g.isPublic,
                        isStaffFavorite = g.isStaffFavorite,
                        DifficultyRating = g.DifficultyRating,
                        SizeRating = g.SizeRating,
                        PopularityRating = g.PopularityRating
                    }
                    ).ToList();

            if (x.Any()) return x;
            else return new List<Geocache>(); // return blank list for error handling
        }


        // GetCachesByCountyName
        //
        // Returns all geocaches within a state using SQL function through Entity Framework.
        // APIController will return JSon to forms.js which asks for geocaching data.
        [HttpGet]
        public List<Geocache> GetCachesByCounty(string countyName)
        {
            GeocacheCentralEntities db = new GeocacheCentralEntities();
            var x = db.findByCounty(countyName)
                    .Select(g => new Geocache
                    {
                        GeocacheID = g.GeocacheID,
                        Latitude = g.Latitude,
                        Longitude = g.Longitude,
                        OwnerID = g.OwnerID,
                        Title = g.Title,
                        Description = g.Description,
                        isPublic = g.isPublic,
                        isStaffFavorite = g.isStaffFavorite,
                        DifficultyRating = g.DifficultyRating,
                        SizeRating = g.SizeRating,
                        PopularityRating = g.PopularityRating
                    }
                    ).ToList();

            if (x.Any()) return x;
            else return new List<Geocache>(); // return blank list for error handling
        }


        // GetCachesByCountyName
        //
        // Returns all geocaches within a state using SQL function through Entity Framework.
        // APIController will return JSon to forms.js which asks for geocaching data.
        //
        // NOTE! lowercase geocache is the database entity, and Geocache is the 
        // C# object.
        [HttpPost]
        public List<Geocache> AddNewGeocache([FromBody]Geocache gData)
        {
            List<Geocache> list = new List<Geocache>();
            GeocacheCentralEntities db = new GeocacheCentralEntities();

            try
            {
                geocache newGeo = new geocache {
                    Latitude = gData.Latitude,
                    Longitude =gData.Longitude,
                    Title = gData.Title,
                    Description = gData.Description,
                    isPublic = true,                // Explicity set values for alpha software
                    isStaffFavorite = true,         // Explicity set values for alpha software
                    OwnerID = 0,                    // Anonymous user, explicity set values for alpha software
                    DifficultyRating = gData.DifficultyRating,
                    SizeRating = gData.SizeRating,
                    PopularityRating = 1
                };
                db.geocaches.Add(newGeo);
                db.SaveChanges();
                list.Add(gData);                    // Return Cache if successful, else return blank list
                return list;
            }
            catch
            {
                // Return null list for jquery to error check against
                return new List<Geocache>();
            }   
        }
    }
}
